const cheerio = require('cheerio');
const lib = require("lib")({token: process.env.STDLIB_TOKEN});
const utils = lib.utils({
  service: 'goddess'
});
const urlencode = require('urlencode');


const Promise = require('bluebird');
const request = require('request');
const rp = Promise.promisify(request.defaults({ jar : true }), { multiArgs : true });

/**
* message event
*
*   All events use this template, simply create additional files with different
*   names to add event responses
*
*   See https://api.slack.com/events-api for more details.
*
* @param {string} user The user id of the user that invoked this event (name is usable as well)
* @param {string} channel The channel id the event was executed in (name is usable as well)
* @param {string} text The text contents of the event
* @param {object} event The full Slack event object
* @param {string} botToken The bot token for the Slack bot you have activated
* @returns {object}
*/
module.exports = (user, channel, text = '', event = {}, botToken = null, callback) => {

  // Only send a response to certain messages
  if (m = text.match(/^K姐賜我(.*)$/)) {
    utils.log['@0.0.6']({
      message: "Matched K姐賜我" // (required)
    }).catch(err => { /* handle error */ })
      .then(result => { /* handle result */ });

    let output = 'Amen! :pray: \n\n';

    if (m && m[1]) {
      const keyword = urlencode(m[1])
      const apiPath = `https://www.googleapis.com/customsearch/v1?key=AIzaSyAi68lZ8voEpWpN3R07RnfGJr6VQOPZcbk&cx=017244791355225797994:b5z9ayauu_m&q=${ keyword }`;
      rp( apiPath ).spread(function (response, body) {
        if (response.statusCode != 200)
            throw new Error('Unsuccessful attempt. Code: ' + response.statusCode);
        return JSON.parse(body);
      }).then(function(resObj) {
        if (resObj.items) {
          let count = 0;
          
          while (count < 5 && resObj.items[count]) {
            count++;
            output += `*${ count } ${ resObj.items[count].title }*\n${ resObj.items[count].formattedUrl }\n\n`
          }

          callback(null, {
            text: output
          });
        }
      }).catch(function(error) {
        callback(null, {
          text: error
        });
      });
    } else {
      callback(null, {
        text: output
      });
    }
  } else if (text.match(/^K姐食乜好\?$/)) {
    utils.log['@0.0.6']({
      message: "Matched K姐食乜好" // (required)
    }).catch(err => { /* handle error */ })
      .then(result => { /* handle result */ });

    callback(null, {
      text: `https://www.openrice.com/zh/hongkong/restaurants/district/%E9%B0%82%E9%AD%9A%E6%B6%8C`
    });
  } else if (text.match(/^K姐天氣好嗎\?/)) {
    utils.log['@0.0.6']({
      message: "Matched K姐天氣好嗎" // (required)
    }).catch(err => { /* handle error */ })
      .then(result => { /* handle result */ });

    const apiPath = "http://api.openweathermap.org/data/2.5/weather?id=1819729&units=metric&APPID=c64b48956d2fb7650051b70880018ec4&lang=zh_tw";
    const obsrvPath = "http://www.hko.gov.hk/m/home_uc.htm";

    Promise.all([
      rp( apiPath ),
      rp( obsrvPath )
    ])
    .spread(function (weather, warning) {
      let output = '';

      if (weather) {
        const resObj = JSON.parse(weather[1]);
        output += `依家氣温係度${ resObj.main.temp }°C, 相對濕度百份之${ resObj.main.humidity }。 ${ resObj.weather[0].description } \n\n`;
      }

      if (warning) {
        const $ = cheerio.load(warning[1]);
        let $warning_img = $('#warning_content').find('img')
        if ($warning_img.length > 0) {
          $warning_img.each(function (i, el) {
            output += $(this).attr('alt') + "\n";
          })
        }
      }

      callback(null, {
        text: output
      });
    })
    .catch(function (error) {
      // Handle any error from all above ste
      callback(null, {
        text: error
      });
    });
  } else if (text.match(/\?$/)) {
    utils.log['@0.0.6']({
      message: "Matched Questionmark" // (required)
    }).catch(err => { /* handle error */ })
      .then(result => { /* handle result */ });

    callback(null, {
      text: `揾我呀?咩事呀`
    });
  } else if (text.match(/happy birthday|hb/i)) {
    utils.log['@0.0.6']({
      message: "Matched Happy birthday" // (required)
    }).catch(err => { /* handle error */ })
      .then(result => { /* handle result */ });

    callback(null, {
      text: "```               ~                  ~\n"+
            "     *                   *                *       *\n"+
            "                  *               *\n"+
            "  ~       *                *         ~    *\n"+
            "              *       ~        *              *   ~\n"+
            "                  )         (         )              *\n"+
            "    *    ~     ) (_)   (   (_)   )   (_) (  *\n"+
            "           *  (_) # ) (_) ) # ( (_) ( # (_)       *\n"+
            "              _#.-#(_)-#-(_)#(_)-#-(_)#-.#\n"+
            "  *         .' #  # #  #  # # #  #  # #  # '.   ~     *\n"+
            "           :   #    #  #  #   #  #  #    #   : \n"+
            "    ~      :.       #     #   #     #       .:      *\n"+
            "        *  | '-.__                     __.-' | *\n"+
            "           |      '''''''''''''''''''''      |         *\n"+
            "     *     |         | ||\\ |˜)|˜)\\ /         |\n"+
            "           |         |˜||˜\\|˜ |˜  |          |       ~\n"+
            "   ~   *   |                                 | * \n"+
            "           |      |˜)||˜)˜|˜| ||˜\\|\\ \\ /     |         *\n"+
            "   *    _.-|      |˜)||˜\\ | |˜|| /|˜\\ |      |-._  \n"+
            "      .'   '.      ˜            ˜           .'   '.  *\n"+
            "      :      '-.__                     __.-'      :\n"+
            "       '.         '''''''''''''''''''''         .'\n"+
            "         '-.._                             _..-'\n"+
            "              '''''''''-----------'''''''''```"
    });
  } else  {
    callback(null, {});
  }
};
